# flutter_exercise

Prueba de Ingreso

Aplicación que consume del Api Rest [https://jsonplaceholder.typicode.com/],
Su función básicamente es listar usuarios y las publicaciones que éstos realizan, todos estos datos son extraídos del servicio Rest anteriormente mencionado.

Al abrir la app se verifica si tenemos los usuarios almacenados de manera local, si es así se listan de lo contrario se consume el servicio web, se almacenan y se muestra.

Cuenta con un buscador por nombre de usuario.

Al seleccionar un usuario se muestra el detalle con la siguiente información

* Nombre
* Teléfono
* Correo
* Publicaciones
    * Título
    * Descripción

## Dependencias

Las depedencias del proyectos son las siguiente:
* animate_do: ^2.1.0
* dio: ^4.0.6
* http_mock_adapter: ^0.3.2
* mockito: ^5.1.0
* path_provider: ^2.0.9
* provider: ^6.0.2
* skeletons: ^0.0.3
* sqflite: ^2.0.2+1
* sqflite_common_ffi: ^2.1.1

## Getting Started

Para instalar las dependecias ejecutar el comando `flutter packages get`
