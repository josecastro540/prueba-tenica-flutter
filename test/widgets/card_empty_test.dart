import 'package:flutter/material.dart';
import 'package:flutter_exercise/widgets/widgets.dart';
import 'package:flutter_test/flutter_test.dart';

void main() {
  testWidgets('cardEmpty title should Jose', (WidgetTester tester) async {
    await tester.pumpWidget(const MaterialApp(
      home: CardEmpty(
        title: 'Jose',
      ),
    ));
    final titleFinder = find.text('Jose');

    expect(titleFinder, findsOneWidget);
  });
}
