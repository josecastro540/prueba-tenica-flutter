import 'package:flutter_exercise/widgets/widgets.dart';
import 'package:flutter_test/flutter_test.dart';

void main() {
  testWidgets('SearchAppBar height should 50', (tester) async {
    expect(const SearchAppBar(height: 50).height, 50);
  });
}
