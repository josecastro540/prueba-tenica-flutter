import 'package:flutter/material.dart';
import 'package:flutter_exercise/models/user.dart';
import 'package:flutter_exercise/widgets/widgets.dart';
import 'package:flutter_test/flutter_test.dart';

void main() {
  testWidgets('CardUser name should Jose Castro', (tester) async {
    final user = User(
      email: 'josecastro540@gmail.com',
      id: 1,
      name: 'Jose Castro',
      phone: '1234567',
      username: 'jcastro540',
    );
    await tester.pumpWidget(
      MaterialApp(
        home: Scaffold(
          body: CardUser(
            user: user,
          ),
        ),
      ),
    );
    final titleFinder = find.text('Jose Castro');
    expect(titleFinder, findsWidgets);
  });
}
