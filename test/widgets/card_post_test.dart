import 'package:flutter/material.dart';
import 'package:flutter_exercise/models/post.dart';
import 'package:flutter_exercise/widgets/card_post.dart';
import 'package:flutter_test/flutter_test.dart';

void main() {
  testWidgets('CardPost title should Title de Prueba', (tester) async {
    final post = PostForUser(userId: 1, id: 1, title: 'Title de Prueba', body: 'body');
    await tester.pumpWidget(MaterialApp(home: CardPost(post: post)));
    final titleFinder = find.text('Title de Prueba');
    expect(titleFinder, findsOneWidget);
  });
}
