import 'package:flutter/material.dart';
import 'package:flutter_exercise/providers/user_provider.dart';
import 'package:flutter_exercise/widgets/post_tiles.dart';
import 'package:flutter_test/flutter_test.dart';
import 'package:provider/provider.dart';

void main() {
  testWidgets('PostTiles should show one title Prueba', (tester) async {
    await tester.pumpWidget(
      MultiProvider(
        providers: [
          ChangeNotifierProvider(
            create: (_) => UserProvider(),
          )
        ],
        child: const MaterialApp(
            home: Scaffold(
                body: CustomScrollView(
          slivers: [PostTiles()],
        ))),
      ),
    );
    final titleFinder = find.text('Title de Prueba');
    expect(titleFinder, findsNothing);
  });
}
