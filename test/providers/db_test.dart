import 'package:flutter_test/flutter_test.dart';
import 'package:sqflite/sqflite.dart';
import 'package:sqflite_common_ffi/sqflite_ffi.dart';

/// Initialize sqflite for test.
void sqfliteTestInit() {
  // Initialize ffi implementation
  sqfliteFfiInit();
  // Set global factory
  databaseFactory = databaseFactoryFfi;
}

Future main() async {
  sqfliteTestInit();
  test('Should create table and save data in sqlite', () async {
    var db = await openDatabase(inMemoryDatabasePath);
    await db.execute('''
      CREATE TABLE UsersTest (
        id INTEGER PRIMARY KEY,
        title TEXT
      )
  ''');
    await db.insert('UsersTest', <String, Object?>{'title': 'UsersTest 1'});
    await db.insert('UsersTest', <String, Object?>{'title': 'UsersTest 2'});

    var result = await db.query('UsersTest');
    expect(result, [
      {'id': 1, 'title': 'UsersTest 1'},
      {'id': 2, 'title': 'UsersTest 2'}
    ]);
    await db.close();
  });
}
