import 'package:dio/dio.dart';
import 'package:flutter_exercise/providers/user_provider.dart';
import 'package:flutter_test/flutter_test.dart';
import 'package:http_mock_adapter/http_mock_adapter.dart';

const successMessage = {'message': 'Success'};
const testPath = 'posts?userId=1';
//modified
void main() {
  final dio = Dio();
  final dioAdapter = DioAdapter(dio: dio);
  var baseUrl = 'https://jsonplaceholder.typicode.com/';

  setUp(() {
    dio.httpClientAdapter = dioAdapter;
  });

  group('- ApiService class methods test', () {
    group('- Get Method', () {
      test('- Get Method Success test', () async {
        dioAdapter.onGet(
          '$baseUrl$testPath',
          (request) {
            return request.reply(200, successMessage);
          },
          data: null,
          queryParameters: {},
          headers: {},
        );

        final service = UserProvider(
          dio: dio,
        );

        final response = await service.getPostsByUser(1);

        expect(response, null);
      });
    });
  });
}
