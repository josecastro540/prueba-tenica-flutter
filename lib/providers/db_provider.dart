import 'dart:io';
import 'package:path/path.dart';
import 'package:path_provider/path_provider.dart';
import 'package:sqflite/sqflite.dart';
import '../models/user.dart';

class DBProvider {
  static Database? _database;
  static final DBProvider db = DBProvider._();
  DBProvider._();

  Future get database async {
    if (_database != null) return _database;
    _database = await initDB();
    return _database;
  }

  Future initDB() async {
    //Path db
    Directory documentsDirectory = await getApplicationDocumentsDirectory();
    final path = join(documentsDirectory.path, 'admissionTest.db');
    // print(path);
    //CREATE DB
    return await openDatabase(
      path,
      version: 1,
      onOpen: (db) {},
      onCreate: (Database db, int version) async {
        await db.execute(''' 
            CREATE TABLE Users (
                id INTEGER PRIMARY KEY,
                name TEXT NOT NULL,
                username TEXT NOT NULL UNIQUE,
                email  TEXT NOT NULL,
                phone TEXT NOT NULL
            );
       ''');
      },
    );
  }

  // Users
  Future addUser(User user) async {
    final db = await database;
    final res = await db.insert('Users', user.toMap(), conflictAlgorithm: ConflictAlgorithm.replace);
    return res;
  }

  Future getUsers() async {
    final db = await database;
    final res = await db.query('Users');
    return res!.isNotEmpty ? res.map((s) => User.fromMap(s)).toList() : [];
  }

  findUser(String name) async {
    final db = await database;
    final res = await db.rawQuery('''
      SELECT 
       id, 
       name, 
       username,
       email,
       phone
      FROM Users
      WHERE name LIKE '%$name%';
    ''');

    return res!.isNotEmpty
        ? res.map((s) => User.fromMap(s)).toList()
        : [
            User(
              id: 0,
              name: 'List is empty',
              username: '',
              email: '',
              phone: '',
            )
          ];
  }
}
