import 'package:flutter/material.dart';
import 'package:dio/dio.dart';
import 'package:flutter_exercise/models/post.dart';
import 'package:flutter_exercise/providers/db_provider.dart';

import '../models/user.dart';
import '../ui/custom_snackbar.dart';

class UserProvider extends ChangeNotifier {
  List<User> users = [];
  List<PostForUser> post = [];
  bool loader = false;
  bool isSearch = false;
  final String _url = 'https://jsonplaceholder.typicode.com';
  final Dio _dioUsers;
  UserProvider({Dio? dio}) : _dioUsers = Dio();

  Future addUser(User user) async {
    final userId = await DBProvider.db.addUser(
      user,
    );
    notifyListeners();
    return userId;
  }

  Future getUsers() async {
    final List result = await DBProvider.db.getUsers();
    users = [...result];
    if (users.isEmpty) {
      loader = true;
      getUserFromApi();
      return;
    }
    loader = false;
    notifyListeners();
    return users;
  }

  Future getUserFromApi() async {
    loader = true;
    final path = '$_url/users';
    notifyListeners();
    try {
      final resp = await _dioUsers.get(path);
      final List row = resp.data;
      for (var u in row) {
        final user = User.fromMap(u);
        addUser(user);
      }
      getUsers();
      loader = false;
    } catch (e) {
      loader = false;
      notifyListeners();
      if (e is DioError) {
        final snack = CustomSnackBar(message: e.error.toString());
        snackbarKey.currentState?.showSnackBar(snack);
      }
    }
  }

  Future getPostsByUser(int id) async {
    post = [];
    loader = true;
    final path = '$_url/posts?userId=$id';

    try {
      final resp = await _dioUsers.get(path);
      final List row = resp.data;
      for (var p in row) {
        final postFromUser = PostForUser.fromMap(p);
        post.add(postFromUser);
      }
      loader = false;
      notifyListeners();
    } catch (e) {
      loader = false;
      notifyListeners();
      if (e is DioError) {
        final snack = CustomSnackBar(message: e.error);
        snackbarKey.currentState?.showSnackBar(snack);
      }
    }
  }

  searchUser(String text) async {
    if (text.isNotEmpty) {
      final List result = await DBProvider.db.findUser(text.trim());
      users = [...result];
    } else {
      getUsers();
    }

    notifyListeners();
  }

  toogleSearch() {
    isSearch = !isSearch;
    if (!isSearch) {
      getUsers();
    }
    notifyListeners();
  }
}
