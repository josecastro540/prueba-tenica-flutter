import 'package:flutter/material.dart';

const ColorScheme colorScheme = ColorScheme(
  primary: Color(0xff218c74),
  secondary: Color(0xFFEFF3F3),
  background: Color(0xfff5f6fa),
  surface: Color(0xFF808080),
  onBackground: Colors.white,
  error: Colors.redAccent,
  onError: Colors.redAccent,
  onPrimary: Colors.white,
  onSecondary: Color(0xFF322942),
  onSurface: Color(0xFF241E30),
  brightness: Brightness.light,
);
