import 'package:flutter/material.dart';

class CustomSnackBar extends SnackBar {
  CustomSnackBar({
    Key? key,
    required String message,
    String btnLabel = 'OK',
    Duration duration = const Duration(seconds: 5),
    VoidCallback? onOk,
  }) : super(
          key: key,
          content: Text(message),
          duration: duration,
          backgroundColor: const Color(0xff218c74),
          action: SnackBarAction(
            textColor: Colors.white,
            label: btnLabel,
            onPressed: () {
              if (onOk != null) {
                onOk();
              }
            },
          ),
        );
}

final GlobalKey<ScaffoldMessengerState> snackbarKey = GlobalKey<ScaffoldMessengerState>();
