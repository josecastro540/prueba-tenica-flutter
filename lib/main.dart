import 'package:flutter/material.dart';
import 'package:flutter_exercise/providers/user_provider.dart';
import 'package:flutter_exercise/screens/home_screen.dart';
import 'package:flutter_exercise/ui/color_scheme.dart';
import 'package:flutter_exercise/ui/ui.dart';

import 'package:provider/provider.dart';

void main() => runApp(const MyApp());

class MyApp extends StatelessWidget {
  const MyApp({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return MultiProvider(
      providers: [
        ChangeNotifierProvider(
          create: (_) => UserProvider(),
        )
      ],
      child: MaterialApp(
        title: 'Prueba de ingreso',
        initialRoute: 'home',
        debugShowCheckedModeBanner: false,
        scaffoldMessengerKey: snackbarKey,
        routes: {
          'home': (context) => const HomeScreen(),
        },
        theme: ThemeData.light().copyWith(
          textTheme: ThemeData().textTheme.apply(
                fontFamily: 'Ubuntu',
              ),
          colorScheme: colorScheme,
        ),
      ),
    );
  }
}
