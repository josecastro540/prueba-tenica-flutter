import 'package:animate_do/animate_do.dart';
import 'package:flutter/material.dart';
import 'package:flutter_exercise/widgets/widgets.dart';
import 'package:provider/provider.dart';

import '../providers/user_provider.dart';

class SearchAppBar extends StatelessWidget implements PreferredSizeWidget {
  final double height;
  const SearchAppBar({Key? key, required this.height}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    final userProvider = Provider.of<UserProvider>(context);
    return AppBar(
      title: userProvider.isSearch
          ? Container(
              padding: const EdgeInsets.only(top: 4),
              child: SlideInRight(
                child: const SearchBar(),
                duration: const Duration(milliseconds: 250),
              ),
            )
          : Container(padding: const EdgeInsets.only(top: 5), child: const Text('Prueba de ingreso')),
      actions: [
        IconButton(
          splashRadius: 0.1,
          icon: userProvider.isSearch ? const Icon(Icons.close) : const Icon(Icons.search),
          onPressed: () {
            userProvider.toogleSearch();
          },
        )
      ],
    );
  }

  @override
  Size get preferredSize => Size.fromHeight(height);
}
