import 'package:flutter/material.dart';
import 'package:skeletons/skeletons.dart';

class SkeletonList extends StatelessWidget {
  const SkeletonList({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    final size = MediaQuery.of(context).size;

    return Container(
      margin: const EdgeInsets.only(
        top: 25,
      ),
      child: ListView(
        children: [
          TileCustomSkeletonCard(size: size),
          TileCustomSkeletonCard(size: size),
          TileCustomSkeletonCard(size: size),
          TileCustomSkeletonCard(size: size),
          TileCustomSkeletonCard(size: size),
        ],
      ),
    );
  }
}

class TileCustomSkeletonCard extends StatelessWidget {
  const TileCustomSkeletonCard({
    Key? key,
    required this.size,
  }) : super(key: key);

  final Size size;

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: const EdgeInsets.all(8.0),
      child: Container(
        padding: const EdgeInsets.all(10),
        margin: const EdgeInsets.all(10),
        decoration: BoxDecoration(
          color: Theme.of(context).colorScheme.onBackground,
          borderRadius: BorderRadius.circular(5),
          boxShadow: const [
            BoxShadow(
              color: Color.fromARGB(99, 87, 96, 111),
              blurRadius: 7,
              offset: Offset(0, 3),
            )
          ],
        ),
        child: SkeletonItem(
          child: Column(
            children: [
              SkeletonParagraph(
                style: SkeletonParagraphStyle(
                  lines: 1,
                  lineStyle: SkeletonLineStyle(
                    height: 18,
                    width: size.width * 0.5,
                  ),
                ),
              ),
              SkeletonParagraph(
                style: SkeletonParagraphStyle(
                  lines: 1,
                  lineStyle: SkeletonLineStyle(
                    height: 15,
                    width: size.width * 0.4,
                  ),
                ),
              ),
              SkeletonParagraph(
                style: SkeletonParagraphStyle(
                  lines: 1,
                  lineStyle: SkeletonLineStyle(
                    height: 15,
                    width: size.width * 0.6,
                  ),
                ),
              ),
              const SizedBox(
                height: 20,
              ),
              Row(
                mainAxisAlignment: MainAxisAlignment.end,
                children: [
                  SkeletonParagraph(
                    style: SkeletonParagraphStyle(
                      lines: 1,
                      lineStyle: SkeletonLineStyle(
                        height: 19,
                        width: size.width * 0.4,
                      ),
                    ),
                  )
                ],
              )
            ],
          ),
        ),
      ),
    );
  }
}
