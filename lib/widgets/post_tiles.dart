import 'package:flutter/material.dart';
import 'package:flutter_exercise/widgets/card_post.dart';
import 'package:provider/provider.dart';

import '../providers/user_provider.dart';

class PostTiles extends StatelessWidget {
  const PostTiles({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    final userProvider = Provider.of<UserProvider>(context);
    return userProvider.loader
        ? SliverList(
            delegate: SliverChildBuilderDelegate(
            (BuildContext context, int index) {
              return Column(
                children: const [
                  SizedBox(
                    height: 300,
                  ),
                  Center(child: CircularProgressIndicator())
                ],
              );
            },
            childCount: 1,
          ))
        : SliverList(
            delegate: SliverChildBuilderDelegate(
              (BuildContext context, int index) {
                return Container(
                  padding: const EdgeInsets.only(
                    left: 20,
                    right: 20,
                  ),
                  child: CardPost(post: userProvider.post[index]),
                );
              },
              childCount: userProvider.post.length,
            ),
          );
  }
}
