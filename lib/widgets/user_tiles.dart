import 'package:flutter/material.dart';
import 'package:flutter_exercise/widgets/widgets.dart';
import 'package:provider/provider.dart';

import '../providers/user_provider.dart';

class UserTiles extends StatelessWidget {
  const UserTiles({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    final userProvider = Provider.of<UserProvider>(context);
    if (userProvider.loader) {
      return const SkeletonList();
    } else {
      return Column(
        children: [
          const SizedBox(
            height: 5,
          ),
          Flexible(
            child: Container(
                padding: const EdgeInsets.only(left: 15, right: 15, bottom: 25),
                child: ListView.builder(
                  itemCount: userProvider.users.length,
                  shrinkWrap: true,
                  clipBehavior: Clip.none,
                  physics: const BouncingScrollPhysics(),
                  itemBuilder: (_, i) => CardUser(user: userProvider.users[i]),
                )),
          )
        ],
      );
    }
  }
}
