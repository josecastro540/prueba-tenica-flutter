import 'package:flutter/material.dart';

class CardEmpty extends StatelessWidget {
  final String title;
  const CardEmpty({Key? key, required this.title}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    final size = MediaQuery.of(context).size;

    return Container(
      width: size.width,
      padding: const EdgeInsets.all(20),
      margin: const EdgeInsets.only(top: 30),
      decoration: BoxDecoration(
        color: Theme.of(context).colorScheme.onBackground,
        borderRadius: BorderRadius.circular(5),
        boxShadow: const [
          BoxShadow(
            color: Color.fromARGB(99, 87, 96, 111),
            blurRadius: 7,
            offset: Offset(0, 3),
          )
        ],
      ),
      child: Row(
        mainAxisAlignment: MainAxisAlignment.center,
        children: [
          Text(
            title,
            style: const TextStyle(fontSize: 17),
          ),
          const SizedBox(width: 10),
          const Icon(Icons.announcement_outlined),
        ],
      ),
    );
  }
}
