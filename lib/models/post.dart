import 'dart:convert';

List<PostForUser> postForUserFromMap(String str) => List<PostForUser>.from(json.decode(str).map((x) => PostForUser.fromMap(x)));

String postForUserToMap(List<PostForUser> data) => json.encode(List<dynamic>.from(data.map((x) => x.toMap())));

class PostForUser {
  PostForUser({
    required this.userId,
    required this.id,
    required this.title,
    required this.body,
  });

  final int userId;
  final int id;
  final String title;
  final String body;

  factory PostForUser.fromMap(Map<String, dynamic> json) => PostForUser(
        userId: json["userId"],
        id: json["id"],
        title: json["title"],
        body: json["body"],
      );

  Map<String, dynamic> toMap() => {
        "userId": userId,
        "id": id,
        "title": title,
        "body": body,
      };
}
