import 'package:flutter/material.dart';
import 'package:flutter_exercise/providers/user_provider.dart';
import 'package:flutter_exercise/widgets/widgets.dart';
import 'package:provider/provider.dart';

class HomeScreen extends StatelessWidget {
  const HomeScreen({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    final userProvider = Provider.of<UserProvider>(context, listen: false);
    userProvider.getUsers();
    return const Scaffold(
      appBar: SearchAppBar(
        height: 70,
      ),
      body: UserTiles(),
    );
  }
}
